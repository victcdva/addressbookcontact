package addressbook;

import java.awt.event.*;
import static java.lang.System.exit;
import java.util.*;
import javax.swing.*;

public class AddressBook {
    public static void main(String[] args) {
        JFrame _mainFrame = new JFrame("Address book");
        _mainFrame.setBounds(100, 100, 430, 389);
        _mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        _mainFrame.getContentPane().setLayout(null);
        
        JLabel _firstName = new JLabel("First Name: ");
        JLabel _lastName = new JLabel("Last Name: ");
        JLabel _phoneNumber = new JLabel("Phone Number: ");
        JLabel _alternateNumber = new JLabel("Alternate Number: ");
        JLabel _email = new JLabel("Email: ");
        JLabel _address = new JLabel("Address: ");
        
        JTextArea _textAddress = new JTextArea();
        
        JButton _cancel = new JButton("Cancel");
        JButton _save = new JButton("Save");
        JButton _clear = new JButton("Clear");
        
        int j = 28;
        JTextField[] contactData = {new JTextField(), new JTextField(), new JTextField(), new JTextField(), new JTextField()};
        
        for (JTextField contactData1 : contactData) {
            contactData1.setBounds(200, j, 120, 17);
            contactData1.setColumns(10);
            _mainFrame.add(contactData1);
            j += 25;
        }
        
        _firstName.setBounds(90, 28, 86, 20); _mainFrame.getContentPane().add(_firstName);
        _lastName.setBounds(90, 50, 86, 20); _mainFrame.getContentPane().add(_lastName);
        _phoneNumber.setBounds(90, 74, 90, 20); _mainFrame.getContentPane().add(_phoneNumber);
        _alternateNumber.setBounds(90, 100, 120, 20); _mainFrame.getContentPane().add(_alternateNumber);
        _email.setBounds(90, 125, 86, 20); _mainFrame.getContentPane().add(_email);
        _address.setBounds(90, 150, 86, 20); _mainFrame.getContentPane().add(_address);
        _textAddress.setBounds(90, 174, 230, 100); _mainFrame.getContentPane().add(_textAddress);
        _textAddress.setColumns(10);

        _cancel.setBounds(90, 280, 75, 20); _mainFrame.add(_cancel);
        _cancel.addActionListener((ActionEvent e) -> {
            exit(0);
        });
        
        _clear.setBounds(168, 280, 75, 20); _mainFrame.add(_clear);
        _clear.addActionListener((ActionEvent e) -> {
           for(JTextField contactField : contactData) {
               contactField.setText("");
           }
           _textAddress.setText("");
        });

        _save.setBounds(245, 280, 75, 20); _mainFrame.add(_save);
        _save.addActionListener((ActionEvent e) -> {
            List<Contact> list = new ArrayList<>();
            Contact contact;
            try {
                String firstName = contactData[0].getText();
                String lastName = contactData[1].getText();
                //int phoneNumber = (contactData[2].getText() != null && !contactData[2].getText().isEmpty()) ? Integer.parseInt(contactData[2].getText()) : 0;
                //int alternateNumber = (contactData[3].getText() != null && !contactData[3].getText().isEmpty()) ? Integer.parseInt(contactData[3].getText()) : 0;
                String phoneNumber = contactData[2].getText();
                String alternateNumber = contactData[3].getText();
                String email = contactData[4].getText();
                String address = _textAddress.getText();
                contact = new Contact(firstName, lastName, phoneNumber, alternateNumber, email, address);

                list.add(contact);
                System.out.println("\nContact array");
                list.forEach(name -> System.out.println(name));
                contact.add(firstName, lastName, phoneNumber, alternateNumber, email, address);
                System.out.println("\nFrom database");
                contact.getAll();

                for(JTextField contactField : contactData) {
                    contactField.setText("");
                }
                _textAddress.setText("");
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(null, "Values aren't correct" + ex.getMessage(), "OK", JOptionPane.PLAIN_MESSAGE);
            }
        });
        
        _mainFrame.setVisible(true);
    }
}
