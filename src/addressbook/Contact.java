package addressbook;

import java.sql.*;
import javax.swing.*;

public class Contact {

    private String _firstName;
    private String _lastName;
    private String _phoneNumber;
    private String _alternateNumber;
    private String _email;
    private String _address;
  
    public String getFirstName() { return _firstName; }
    public void setFirstName(String _firstName) { this._firstName = _firstName; }
    public String getLastName() { return _lastName; }
    public void setLastName(String _lastName) { this._lastName = _lastName; }
    public String getPhoneNumber() { return _phoneNumber; }
    public void setPhoneNumber(String _phoneNumber) { this._phoneNumber = _phoneNumber; }
    public String getAlternateNumber() { return _alternateNumber; }
    public void setAlternateNumber(String _alternateNumber) { this._alternateNumber = _alternateNumber; }
    public String getEmail() { return _email; }
    public void setEmail(String _email) { this._email = _email; }
    public String getAddress() { return _address; }
    public void setAddress(String _address) { this._address = _address; }

    public Contact() {
        this._firstName = "";
        this._lastName = "";
        this._phoneNumber = "";
        this._alternateNumber = "";
        this._email = "";
        this._address = "";
    }
    
    public Contact(String firstName, String lastName, String phoneNumber, String alternateNumber, String email, String address) {
        this._firstName = firstName;
        this._lastName = lastName;
        this._phoneNumber = phoneNumber;
        this._alternateNumber = alternateNumber;
        this._email = email;
        this._address = address;
    }
    
    public void add(String firstName, String lastName, String phoneNumber, String alternateNumber, String email, String address) {
        MysqlConnection mc = new MysqlConnection();
        Connection connection = mc.Connection();
        try {
            String query = "insert into contacts (firstName, lastName, phoneNumber, alternateNumber, email, address) values (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, phoneNumber);
            statement.setString(4, alternateNumber);
            statement.setString(5, email);
            statement.setString(6, address);
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Contact was added successfully.", "OK", JOptionPane.PLAIN_MESSAGE);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Failed to add contact. " + e.getMessage(), "OK", JOptionPane.PLAIN_MESSAGE);
        }
    }
    
    public void getAll() {
        MysqlConnection mc = new MysqlConnection();
        Connection connection = mc.Connection();
        try {
            String query = "select firstName, lastName, phoneNumber, alternateNumber, email, address from contacts order by firstName;";
            PreparedStatement statement = connection.prepareStatement(query);
            //System.out.print(statement);
            ResultSet result = statement.executeQuery(query);
            while(result.next()) {
                    String firstName = result.getString("firstName");
                    String lastName = result.getString("lastName");
                    String phoneNumber = result.getString("phoneNumber");
                    String alternateNumber = result.getString("alternateNumber");
                    String email = result.getString("email");
                    String address = result.getString("address");
                    System.out.println(firstName + " " + lastName + " " + phoneNumber + " " + alternateNumber + " " + email + " " + address);
                }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Failed to get contacts. " + e.getMessage(), "OK", JOptionPane.PLAIN_MESSAGE);
        } 
    }

    @Override
    public String toString() {
        return "First name: " + _firstName + " " + "Last name: " + _lastName + " " + "Phone number: " +_phoneNumber + " " + "Alternate number: " +_alternateNumber + " " + "Email: " + _email + " " + "Address: " + _address + " ";
    }
}