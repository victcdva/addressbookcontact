package addressbook;

import java.sql.*;
import javax.swing.*;

public class MysqlConnection {
    private static final String _url = "jdbc:mysql://localhost:3306/addressbook?&useSSL=false";    
    private static final String _driverName = "com.mysql.jdbc.Driver";   
    private static final String _username = "root";   
    private static final String _password = "root";
    private static Connection _connection;
 
    public Connection Connection() {
        try {
            Class.forName(_driverName);
            try {
                _connection = DriverManager.getConnection(_url, _username, _password);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Failed to create the database connection. " + ex.getMessage(), "OK", JOptionPane.PLAIN_MESSAGE);
            }
        } catch (Exception ex) {            
            JOptionPane.showMessageDialog(null, "Driver not found. " + ex.getMessage(), "OK", JOptionPane.PLAIN_MESSAGE);
        }
        return _connection;
    }
}
